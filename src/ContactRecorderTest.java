import org.junit.jupiter.api.*;

class ContactManagerTest {

    ContactRecorder contactManager;
    String firstName;
    String middleName;
    String lastName;
    String phoneNumber;
    String email;

    @BeforeEach
    public void setupEach() {
        contactManager = new ContactRecorder();
        firstName = "James";
        middleName = "Carlton";
        lastName = "Brown";
        phoneNumber = "999919999";
        email = "contact@company.com";
    }

    @Test
    @DisplayName("Should create a contact successfully when params are correct")
    public void shouldCreateContactSuccessfully() {
        contactManager.addContact(firstName, middleName, lastName, phoneNumber, email);

        Assertions.assertFalse(contactManager.getAllContacts().isEmpty());
        Assertions.assertEquals(1, contactManager.getAllContacts().size());

        Assertions.assertTrue(contactManager.getAllContacts().stream()
                .anyMatch(contact -> contact.getFirstName().equals(firstName) &&
                        contact.getLastName().equals(lastName) &&
                        contact.getMiddleName().equals(middleName) &&
                        contact.getPhoneNumber().equals(phoneNumber) &&
                        contact.getEmail().equals(email)));
    }

    @Nested
    class ParamNotProvidedTest {
        @Test
        @DisplayName("Should not create the contact entry when first name is not provided")
        public void shouldThrowExceptionWhenFirstNameIsNotProvided() {
            firstName = null;

            Assertions.assertThrows(RuntimeException.class, () -> contactManager.addContact(firstName, middleName, lastName, phoneNumber, email));
        }

        @Test
        @DisplayName("Should not create the contact entry when middle name is not provided")
        public void shouldThrowExceptionWhenMiddleNameIsNotProvided() {
            String middleName = null;

            Assertions.assertThrows(RuntimeException.class, () -> contactManager.addContact(firstName, middleName, lastName, phoneNumber, email));
        }

        @Test
        @DisplayName("Should not create the contact entry when last name is not provided")
        public void shouldThrowExceptionWhenLastNameIsNotProvided() {
            String lastName = null;

            Assertions.assertThrows(RuntimeException.class, () -> contactManager.addContact(firstName, middleName, lastName, phoneNumber, email));
        }

        @Test
        @DisplayName("Should not create the contact entry when phone number is not provided")
        public void shouldThrowExceptionWhenPhoneNumberIsNotProvided() {
            String phoneNumber = null;

            Assertions.assertThrows(RuntimeException.class, () -> contactManager.addContact(firstName, middleName, lastName, phoneNumber, email));
        }

        @Test
        @DisplayName("Should not create the contact entry when phone number has more than 9 digits")
        public void shouldThrowExceptionWhenPhoneNumberIsLonger() {
            String phoneNumber = "9876543210";

            Assertions.assertThrows(RuntimeException.class, () -> contactManager.addContact(firstName, middleName, lastName, phoneNumber, email));
        }

        @Test
        @DisplayName("Should not create the contact entry when phone number does not start with a 9")
        public void shouldThrowExceptionWhenPhoneNumberStartsWithWrongDigit() {
            String phoneNumber = "123456789";

            Assertions.assertThrows(RuntimeException.class, () -> contactManager.addContact(firstName, middleName, lastName, phoneNumber, email));
        }

        @Test
        @DisplayName("Should not create the contact entry when phone number includes invalid characters")
        public void shouldThrowExceptionWhenPhoneNumberIncludeInvalidCharacters() {
            String phoneNumber = "98765432A";

            Assertions.assertThrows(RuntimeException.class, () -> contactManager.addContact(firstName, middleName, lastName, phoneNumber, email));
        }

        @Test
        @DisplayName("Should not create the contact entry when email is not provided")
        public void shouldThrowExceptionWhenEmailIsNotProvided() {
            String email = null;

            Assertions.assertThrows(RuntimeException.class, () -> contactManager.addContact(firstName, middleName, lastName, phoneNumber, email));
        }

        @Test
        @DisplayName("Should not create the contact entry when email does not include @")
        public void shouldThrowExceptionWhenEmailIsNotValid() {
            String email = "notvalidemail.com";

            Assertions.assertThrows(RuntimeException.class, () -> contactManager.addContact(firstName, middleName, lastName, phoneNumber, email));
        }
    }
}