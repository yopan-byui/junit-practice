import java.util.regex.Pattern;

public class Contact {
    private String firstName;
    private String middleName;
    private String lastName;
    private String phoneNumber;
    private String email;


    public Contact(String firstName, String middleName, String lastName, String phoneNumber, String email) {
        this.firstName = firstName;
        this.middleName = middleName;
        this.lastName = lastName;
        this.phoneNumber = phoneNumber;
        this.email = email;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void validateFirstName() {
        if(this.firstName.isBlank()) {
            throw new RuntimeException("First name cannot be null or empty");
        }
    }

    public void validateMiddleName() {
        if(this.middleName.isBlank()) {
            throw new RuntimeException("Middle name cannot be null or empty");
        }
    }

    public void validateLastName() {
        if(this.lastName.isBlank()) {
            throw new RuntimeException("Last name cannot be null or empty");
        }
    }

    public void validatePhoneNumber() {
        if(this.phoneNumber.isBlank()) {
            throw new RuntimeException("Phone number cannot be null or empty");
        }

        if(!(this.phoneNumber.length() == 9)) {
            throw new RuntimeException("Phone number should be 9 digits long");
        }

        if(!this.phoneNumber.matches("\\d+")) {
            throw new RuntimeException("Phone number contains invalid entries");
        }

        if(!this.phoneNumber.startsWith("9")) {
            throw new RuntimeException("Phone number should start with a 9");
        }
    }

    public void validateEmail() {
        if(this.email.isBlank()) {
            throw new RuntimeException("Last name cannot be null or empty");
        }

        if(!emailStringIncludesSymbol()) {
            throw new RuntimeException("Email must include @");
        }
    }

    private boolean emailStringIncludesSymbol() {
        String regexPattern = "^(.+)@(\\S+)$";
        return Pattern.compile(regexPattern)
                .matcher(this.email)
                .matches();
    }
}
