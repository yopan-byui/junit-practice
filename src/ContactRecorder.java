import java.util.Collection;
import java.util.Set;
import java.util.HashSet;

public class ContactRecorder {

    Set<Contact> contactList = new HashSet<>();

    public void addContact(String firstName, String middleName, String lastName, String phoneNumber, String email) {
        Contact contact = new Contact(firstName, middleName, lastName, phoneNumber, email);

        validateContact(contact);
        contactList.add(contact);
    }

    public Collection<Contact> getAllContacts() {
        return contactList;
    }

    private void validateContact(Contact contact) {
        contact.validateFirstName();
        contact.validateMiddleName();
        contact.validateLastName();
        contact.validatePhoneNumber();
        contact.validateEmail();
    }
}
